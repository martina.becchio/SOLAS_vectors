//curvefitting
/****************************************************************************
**
** Copyright (C) 2019 Stryker Ireland
**
** \brief Class for fitting Gaussian curves etc to data.
** \file curvefitter.h
****************************************************************************/
#ifndef CURVEFITTER_H
#define CURVEFITTER_H

#include <stdio.h>
#include <initializer_list>
#include <iostream>
#include <random>
#include <cmath>
#include <vector>
//#include <QObject>
//#include <QtCharts/QAbstractSeries>
//#include <QtCharts/QXYSeries>
#include <iostream>
#include <cmath>

//#include <QVector>
//#include <QCoreApplication> //to keep gui responsive
using namespace std;
//QT_BEGIN_NAMESPACE
//class QQuickView; //this is for the Qt user interface
//QT_END_NAMESPACE
//QT_CHARTS_USE_NAMESPACE


extern int g_polyorder;
extern int g_gaussNumGaussians;

const int linefit1 = 18;
const int saturationDetection = 1;
const int linefit2 = 39;
const int Glim1 = 0;
const int Glim2 = linefit2-linefit1;
// SPI Comms
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

// Struct to hold Gaussian peak results values for convenience
struct gaussPeaks{
    double peakCentre;
    double peakAmplitude;
    double peakWidth;
};

class curveFitter
{
public:
    //explicit curveFitter(int gaussNumGaussians=1, int polyOrder=2, int minWavelengthIndex=122, int maxWavelengthIndex=187);
    explicit curveFitter(int polyOrder=1, int minWavelengthIndex=105, int maxWavelengthIndex=231);

//signals:
    //void m_polyBaselineFiChanged();
    //void m_gaussianFluorophoreFitChanged();

public:
    // Main fitting access functions
    double f_performFits(bool baseline=true, bool gaussian=true);
    // Gets
    vector<double> get_rawDataX() { return m_rawCurveData; }
    vector<double> get_polyBaselineFit(){ return m_polyBaselineFit; }
    vector<double> get_rawMinusBaseline() { return m_rawMinusBaseline; }
    vector<double> get_gaussFluorFit() { return m_gaussianFluorophoreFit; }
    double get_fluorMeasurement() { return m_fluorMeasurement; }
    int get_numberOfDataPoints(int* startIndex=nullptr, int* endIndex=nullptr);
    vector<double>* get_GaussianParams(){ return &m_gaussianParameters; }
    //int get_fluorophoreType(){return m_fluorophoreType;}

    // Sets
    int set_rawData(vector<int>, int arraySize);
    void set_polyBaselineOrder(int order);
    //void set_gaussNumberOfGaussians(int numGaussians);
    void set_fluorophoreType(int typeVal);
    void set_fluorescenceMeausurementCalculationParameters(double peak1Freq, double peak1Width);
    double norm_pdf(double x, double mu, double sigma, double amp);
    double f_calculateFlMeasurement();
    vector<float> readHamaData();
    // Results Data for Access Externally
    vector<gaussPeaks> m_gaussPeakResults = vector<gaussPeaks>(1);

private:
    static double f_gaussianMixtureEval(const double x, const double* params); // This is static so that a reference to it can be passed to the minimisation function
    static double f_polynomialEval(const double x, const double* params);
    int f_setInitialParamGuesses(bool poly=true, bool gauss=true);
    int f_wavelengthGenerateWavelengthArray();

    vector<double> f_fitPolynomial(vector<double>* curvePointsX, vector<double>* curvePointsY, vector<double>* params, vector<double>* xWindow, vector<double>* yWindow);
    int f_fitGaussians(vector<double> *curvePointsX, vector<double> *curvePointsY, vector<double> *params);

private:
    //int m_fluorophoreType;  // 0=PPIX, 1=ICG

    int m_numberOfWavelengthPoints;
    int m_wavelengthFitMin;
    int m_wavelengthFitMax;
    int saturation = 0;

    vector<double> m_polyParameters;
    int m_polyNumIterations;    //number of baseline polynomial iterations
    vector<double> m_gaussianParameters = vector<double>(3);
    int m_gaussNumIterations;   // Maximum number of gaussian fit iterations

    vector<double> m_wavelengthValues;
    vector<double> m_rawCurveData;
    vector<double> m_rawCurveData_win;
    const int len = linefit2+1-linefit1;
    vector<double> xWindow = vector<double> (len);
    vector<double> yWindow = vector<double> (len);
    vector<double> xWindow_gauss;
    vector<double> yWindow_gauss;
    vector<double> xWindow_poly;
    vector<double> yWindow_poly;
    vector<double> xWindow_poly_right;
    vector<double> yWindow_poly_right;
    vector<double> xWindow_poly_left;
    vector<double> yWindow_poly_left;
    vector<double> pdf;
    vector<double> m_polyBaselineFit;
    vector<double> m_rawMinusBaseline;
    vector<double> m_gaussianFluorophoreFit;
    double m_fluorMeasurement;

    double m_fluorPeak1FreqMin, m_fluorPeak1FreqMax;
    double m_fluorPeak2FreqMin, m_fluorPeak2FreqMax;
    double m_fluorPeak1WidthMin, m_fluorPeak1WidthMax;
    double m_fluorPeak2WidthMin, m_fluorPeak2WidthMax;
    //these will have to be taken from the specific spectrometer
    float hamaData_A0;
    float hamaData_B1;
    float hamaData_B2;
    float hamaData_B3;
    float hamaData_B4;
    float hamaData_B5;
    string coeff;
    vector<float> hamaData;
    double calcL, calcM, calcH, errorL, errorM, errorH, maxErr;

    // TEST FUNCTIONALITY
public:
    int f_testCurveFitter();
    int m_testNumFitsPerformed = 0;
    clock_t m_testTotalProcTimeForPolyFits = 0;
    clock_t m_testTotalProcTimeForGaussFits = 0;

public:
    void startCalibration();
    void cancelCalibration();
    void endCalibration();
    void processCalibrationData();

public:
    int specCount;
    vector<double> row;
    string line, word;
    string colname;
    vector<double> calData;
    vector<double> accumSpec;
    vector<double> averageSpec;
    vector<double> normSpec;
    vector<double> invSpec;
    vector<double> calVals;

};

#endif