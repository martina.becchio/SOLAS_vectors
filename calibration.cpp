#include "curvefitter.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include "lmcurve.h"
#include "testdata.h"
#include <time.h>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <initializer_list>
#include <random>
#include "calibration.h"
#include <sstream>
using namespace std;

// first three are mainly for GUI to start or stop the calibration so will write them later

void calibration::startCalibration()
{
    /*global picSave, dataWrite, file1, fileCSV, calibrating, calibrationNo, numSpecsCalibration
    calibrationNo = 0
    calibrating = 1
    file1 = open("/home/pi/Desktop/Calibration/SolasRev04System05CalibrationData.csv", "w", 1)
    fileCSV = csv.writer(file1, delimiter = ',')
    winStartandCal.activateWindow()
    winStartandCal.show()
    buttonDC.hide()
    buttonDemo.hide()
    buttonZeiss.hide()
    buttonCalibrating.show()
    buttonCalibratingCancel.show()*/
}    
    
void calibration::cancelCalibration()
{
    /*global calibrationNo, numSpecsCalibration
    calibrationNo = numSpecsCalibration
    endCalibration()*/
}

void calibration::endCalibration()
{
    /*global dataWrite, file1, calibrationNo, calibrating, numSpecsCalibration
    calibrating = 0
    if calibrationNo < numSpecsCalibration:
        calibrationNo = numSpecsCalibration
    file1.close()
    dataWrite=0
    processCalibrationData()
    buttonCalibrating.hide()
    buttonCalibratingCancel.hide()
    buttonDC.show()
    buttonDemo.show()
    buttonZeiss.show()*/
}

void calibration::processCalibrationData()
{   
    ifstream cal("C:\\Users\\mbecchio\\OneDrive - Stryker\\Desktop\\R&TD\\SOLAS\\SolasRev04System05CalibrationData.csv");
    int specCount = 1;
    if(cal.is_open())
    {
        while(getline(cal, line))
        {
            stringstream str(line);
            while(getline(str, coeff, ','))
            {
                if (stof(coeff)<0.01)
                    coeff="0.01";
                calData.push_back(stof(coeff));

            } 
            if (specCount == 1)
            {
                for (int k=0; k<400; ++k)
                {
                    accumSpec.push_back(calData[k]);
                }
            }    
            else 
            for (int k=0; k<400; ++k)
            {
                accumSpec[k] += calData[k];
            }
            ++specCount;
        }
    }
    else
        cout<<"Could not open the file\n";
    --specCount;
    for (int i=0; i<accumSpec.size(); ++i) 
    {
        averageSpec.push_back(accumSpec[i]/specCount);
    }
    double avg = (std::accumulate(averageSpec.begin(), averageSpec.end(), 0.0)) / averageSpec.size();
    for (int i=0; i<averageSpec.size(); ++i)
    {
        normSpec.push_back(averageSpec[i]/avg);
    }
    for (int i=0; i<normSpec.size(); ++i)
    {
        invSpec.push_back(1/normSpec[i]);
    }
    for (int i=0; i<400; ++i)
    {
        if(i<100||i>360)
            invSpec[i] = 0;
    }

    ofstream w_cal("C:\\Users\\mbecchio\\OneDrive - Stryker\\Desktop\\R&TD\\SOLAS\\SolasRev04System05Calibration.csv");
    if (w_cal.is_open())
    {
        for(int k=0; k<400; ++k)
        {
            w_cal << invSpec[k] << ",";
        }
        
    }
    /*file1.close()
    readCalibrationFile()
    winSettings.activateWindow()*/
}
vector<double> calibration::readCalibrationFile()
{
    ifstream cal("C:\\Users\\mbecchio\\OneDrive - Stryker\\Desktop\\R&TD\\SOLAS\\SolasRev04System05Calibration.csv");
    if(cal.is_open())
    {
        while(getline(cal, line))
        {
            stringstream str(line);
            while(getline(str, coeff, ','))
            {
                calVals.push_back(stof(coeff));

            } 
        }
    }
    else
        cout<<"Could not open the file\n";
    return calVals;
}